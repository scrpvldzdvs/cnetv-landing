const { series, src, watch, dest } = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass');
const copy = require('gulp-copy');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const imagemin = require('gulp-imagemin');
const buildFolder = 'build';

const cleanAllTask = () => {
  return src('build/**/').pipe(clean());
};

const cleanCSSTask = () => {
  return src('build/css/**').pipe(clean());
};

const copyTask = () => {
  return src(['src/favicon/**', 'src/pages/**']).pipe(dest(buildFolder));
};

function styleTask() {
  return src('src/sass/app.scss')
    .pipe(sass({ includePaths: ['node_modules'] }))
    .pipe(autoprefixer())
    .pipe(cleanCSS())
    .pipe(rename('style.css'))
    .pipe(dest(`${buildFolder}/css`));
  //.pipe(notify('Style build successful'));
}

function imageTask() {
  return src('src/images/**')
    .pipe(imagemin())
    .pipe(dest(`${buildFolder}/images`));
}

function watcherTask() {
  watch(
    ['src/**/*.html'],
    { delay: 1000, ignoreInitial: false },
    series(copyTask)
  );
  watch(
    ['src/images'],
    { delay: 1000, ignoreInitial: false },
    series(imageTask)
  );
  watch(
    ['src/sass'],
    { delay: 1000, ignoreInitial: false },
    series(cleanCSSTask, styleTask)
  );
}

module.exports = {
  default: series(cleanAllTask, imageTask, styleTask, copyTask),
  watch: watcherTask,
};
